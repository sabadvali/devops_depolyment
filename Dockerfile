FROM python:3.7

RUN mkdir /devops

WORKDIR /devops/

COPY . /devops/

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 8888

CMD [ "python", "app.py"]

