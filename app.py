from flask import Flask


app = Flask(__name__)


@app.route('/')
def HelloWorld():
    return 'hello world from azure VM update:v5 is working <3.!'


app.run(host='0.0.0.0', port=8888)
